/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jainch
 */
public class BookDao {

    String[] coln = {"ID", "Name", "Amount", "Price", "Receive Date", "Type"};
    DefaultTableModel model = new DefaultTableModel(coln, 0) {
    };

    Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:D:/book_store/db/book_store.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return conn;
    }

    public boolean checkAddBook(String book) {
        String sql = "SELECT Book_id FROM Book";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                String id = rs.getString("Book_id");
                return book.equals(id);
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public Book selectForEdit(String book) {
        String sql = "SELECT Book_id, Book_name, Book_amount, Book_price, Book_date_recieve, Book_type FROM Book";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Book_id");
                String Name = rs.getString("Book_name");
                int Amount = rs.getInt("Book_amount");
                Double Price = rs.getDouble("Book_price");
                String Date = rs.getString("Book_date_recieve");
                String Type = rs.getString("Book_type");
                if (book.equals(ID)) {
                    Book arr = new Book(ID, Name, Amount, Price, Date, Type);
                    return arr;
                }

            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public DefaultTableModel resetModel() {
        model.setRowCount(0);
        return model;
    }

    public DefaultTableModel searchByName(String name) {
        String sql = "SELECT Book_id, Book_name, Book_amount, Book_price, Book_date_recieve, Book_type FROM Book WHERE Book_name = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, name);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Book_id");
                String Name = rs.getString("Book_name");
                int Amount = rs.getInt("Book_amount");
                Double Price = rs.getDouble("Book_price");
                String Date = rs.getString("Book_date_recieve");
                String Type = rs.getString("Book_type");
                model.addRow(new Object[]{ID, Name, Amount, Price, Date, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public DefaultTableModel searchByType(String type) {
        String sql = "SELECT Book_id, Book_name, Book_amount, Book_price, Book_date_recieve, Book_type FROM Book WHERE Book_type = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, type);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Book_id");
                String Name = rs.getString("Book_name");
                int Amount = rs.getInt("Book_amount");
                Double Price = rs.getDouble("Book_price");
                String Date = rs.getString("Book_date_recieve");
                String Type = rs.getString("Book_type");
                model.addRow(new Object[]{ID, Name, Amount, Price, Date, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public DefaultTableModel selectForShowTable() {
        String sql = "SELECT Book_id, Book_name, Book_amount, Book_price, Book_date_recieve, Book_type FROM Book";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Book_id");
                String Name = rs.getString("Book_name");
                int Amount = rs.getInt("Book_amount");
                Double Price = rs.getDouble("Book_price");
                String Date = rs.getString("Book_date_recieve");
                String Type = rs.getString("Book_type");
                model.addRow(new Object[]{ID, Name, Amount, Price, Date, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public void updateBook(String Book_id, String Book_name, int Book_amount, double Book_price, String Book_date_recieve, String Book_type) {
        String sql = "UPDATE Book SET Book_name = ? , " + "Book_amount = ? ," + "Book_price = ? ,"
                + "Book_date_recieve = ? ," + "Book_type = ?"
                + "WHERE Book_id = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, Book_name);
            pstmt.setInt(2, Book_amount);
            pstmt.setDouble(3, Book_price);
            pstmt.setString(4, Book_date_recieve);
            pstmt.setString(5, Book_type);
            pstmt.setString(6, Book_id);
            // updateBook 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void insertBook(String Book_id, String Book_name, int Book_amount, double Book_price, String Book_date_recieve, String Book_type) {
        String sql = "INSERT INTO Book(Book_id, Book_name, Book_amount, Book_price, Book_date_recieve, Book_type) VALUES(?,?,?,?,?,?)";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, Book_name);
            pstmt.setInt(2, Book_amount);
            pstmt.setDouble(3, Book_price);
            pstmt.setString(4, Book_date_recieve);
            pstmt.setString(5, Book_type);
            pstmt.setString(6, Book_id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void deleteBook(String Book_id) {
        String sql = "DELETE FROM Book WHERE Book_id = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, Book_id);
            // execute the deleteBook statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

}
