/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jainch
 */
public class PartnerDao {

    String[] coln = {"ID", "Name", "Address", "Tel"};
    DefaultTableModel model = new DefaultTableModel(coln, 0) {
    };

    Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:D:/book_store/db/book_store.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return conn;
    }

    public boolean checkAddPartner(String partner) {
        String sql = "SELECT Partner_tel FROM Partner";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                String id = rs.getString("Partner_tel");
                return partner.equals(id);
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public Partner selectForEdit(String partner) {
        String sql = "SELECT Partner_id, Partner_name, Partner_address, Partner_tel FROM Partner";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Partner_id");
                String Name = rs.getString("Partner_name");
                String Address = rs.getString("Partner_address");
                String Tel = rs.getString("Partner_tel");
                if (partner.equals(ID)) {
                    Partner arr = new Partner(ID, Name, Address, Tel);
                    return arr;
                }

            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public DefaultTableModel resetModel(){
        model.setRowCount(0);
        return model;
    }
    
    public DefaultTableModel searchByName(String name) {
        String sql = "SELECT Partner_id, Partner_name, Partner_address, Partner_tel FROM Partner WHERE Partner_name = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, name);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Partner_id");
                String Name = rs.getString("Partner_name");
                String Address = rs.getString("Partner_address");
                String Tel = rs.getString("Partner_tel");
                model.addRow(new Object[]{ID, Name, Address, Tel});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public DefaultTableModel searchByTel(String tel) {
        String sql = "SELECT Partner_id, Partner_name, Partner_address, Partner_tel FROM Partner WHERE Partner_tel = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, tel);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Partner_id");
                String Name = rs.getString("Partner_name");
                String Address = rs.getString("Partner_address");
                String Tel = rs.getString("Partner_tel");
                model.addRow(new Object[]{ID, Name, Address, Tel});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }
    
    public DefaultTableModel selectForShowTable() {
        String sql = "SELECT Partner_id, Partner_name, Partner_address, Partner_tel FROM Partner";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Partner_id");
                String Name = rs.getString("Partner_name");
                String Address = rs.getString("Partner_address");
                String Tel = rs.getString("Partner_tel");
                model.addRow(new Object[]{ID, Name, Address, Tel});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public void updatePartner(String Partner_id, String Partner_name, String Partner_address, String Partner_tel) {
        String sql = "UPDATE Partner SET Partner_name = ? , " + "Partner_address = ? ," + "Partner_tel = ?"
                + "WHERE Partner_id = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, Partner_name);
            pstmt.setString(2, Partner_address);
            pstmt.setString(3, Partner_tel);
            pstmt.setString(4, Partner_id);
            // updatePartner 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void insertPartner(String Partner_id, String Partner_name, String Partner_address, String Partner_tel) {
        String sql = "INSERT INTO Partner(Partner_id, Partner_name, Partner_address, Partner_tel) VALUES(?,?,?,?)";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, Partner_id);
            pstmt.setString(2, Partner_name);
            pstmt.setString(3, Partner_address);
            pstmt.setString(4, Partner_tel);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void deletePartner(String Partner_id) {
        String sql = "DELETE FROM Partner WHERE Partner_id = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, Partner_id);
            // execute the deletePartner statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

}
