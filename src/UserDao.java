/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jainch
 */
public class UserDao {

    static boolean foundId = false, foundPass = false;
    String[] coln = {"ID", "Tel", "Password", "Firstname", "Lastname", "Gender", "Address", "Type"};
    DefaultTableModel model = new DefaultTableModel(coln, 0) {
    };

    Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:D:/book_store/db/book_store.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return conn;
    }

    public boolean checkAddUser(String tel) {
        String sql = "SELECT User_tel FROM User";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                String id = rs.getString("User_tel");
                return tel.equals(id);
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public int checkLogin(String recId, String recPass) {
        String sql = "SELECT User_tel, User_password FROM User";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                String id = rs.getString("User_tel");
                String password = rs.getString("User_password");
                if (recId.equals(id)) {
                    foundId = true;
                    if (password.equals(password)) {
                        foundPass = true;
                        break;
                    } else {
                        foundPass = false;
                        break;
                    }
                } else {
                    foundId = false;
                }
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return checkLoginFound(foundId, foundPass);
    }

    public int checkLoginFound(boolean foundId, boolean foundPass) {
        if (foundId == true && foundPass == true) {
            return 1;
        } else if (foundId == true && foundPass == false) {
            return 2;
        } else {
            return 3;
        }
    }

    public User selectForEdit(String user) {
        String sql = "SELECT User_id, User_tel, User_password, User_firstname, User_lastname, User_gender, User_address, User_type FROM User";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("User_id");
                String Tel = rs.getString("User_tel");
                String Password = rs.getString("User_password");
                String Firstname = rs.getString("User_firstname");
                String Lastname = rs.getString("User_lastname");
                String Gender = rs.getString("User_gender");
                String Address = rs.getString("User_address");
                String Type = rs.getString("User_type");
                if (user.equals(ID)) {
                    User arr = new User(ID, Tel, Password, Firstname, Lastname, Gender, Address, Type);
                    return arr;
                }

            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public DefaultTableModel resetModel() {
        model.setRowCount(0);
        return model;
    }

    public DefaultTableModel searchByFname(String fname) {
        String sql = "SELECT User_id, User_tel, User_password, User_firstname, User_lastname, User_gender, User_address, User_type FROM User WHERE User_firstname = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, fname);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("User_id");
                String Tel = rs.getString("User_tel");
                String Password = rs.getString("User_password");
                String Firstname = rs.getString("User_firstname");
                String Lastname = rs.getString("User_lastname");
                String Gender = rs.getString("User_gender");
                String Address = rs.getString("User_address");
                String Type = rs.getString("User_type");
                model.addRow(new Object[]{ID, Tel, Password, Firstname, Lastname, Gender, Address, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public DefaultTableModel searchByTel(String tel) {
        String sql = "SELECT User_id, User_tel, User_password, User_firstname, User_lastname, User_gender, User_address, User_type FROM User WHERE User_tel = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, tel);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("User_id");
                String Tel = rs.getString("User_tel");
                String Password = rs.getString("User_password");
                String Firstname = rs.getString("User_firstname");
                String Lastname = rs.getString("User_lastname");
                String Gender = rs.getString("User_gender");
                String Address = rs.getString("User_address");
                String Type = rs.getString("User_type");
                model.addRow(new Object[]{ID, Tel, Password, Firstname, Lastname, Gender, Address, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public DefaultTableModel searchByType(String type) {
        String sql = "SELECT User_id, User_tel, User_password, User_firstname, User_lastname, User_gender, User_address, User_type FROM User WHERE User_type = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, type);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("User_id");
                String Tel = rs.getString("User_tel");
                String Password = rs.getString("User_password");
                String Firstname = rs.getString("User_firstname");
                String Lastname = rs.getString("User_lastname");
                String Gender = rs.getString("User_gender");
                String Address = rs.getString("User_address");
                String Type = rs.getString("User_type");
                model.addRow(new Object[]{ID, Tel, Password, Firstname, Lastname, Gender, Address, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public DefaultTableModel selectForShowTable() {
        String sql = "SELECT User_id, User_tel, User_password, User_firstname, User_lastname, User_gender, User_address, User_type FROM User";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("User_id");
                String Tel = rs.getString("User_tel");
                String Password = rs.getString("User_password");
                String Firstname = rs.getString("User_firstname");
                String Lastname = rs.getString("User_lastname");
                String Gender = rs.getString("User_gender");
                String Address = rs.getString("User_address");
                String Type = rs.getString("User_type");
                model.addRow(new Object[]{ID, Tel, Password, Firstname, Lastname, Gender, Address, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public void updateUser(String User_id, String User_tel, String User_password, String User_fname, String User_lname, String User_gender, String User_address, String User_type) {
        String sql = "UPDATE User SET User_tel = ? , " + "User_password = ? ," + "User_firstname = ? ,"
                + "User_lastname = ? ," + "User_gender = ? ," + "User_address = ? ," + "User_type = ?"
                + "WHERE User_id = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, User_tel);
            pstmt.setString(2, User_password);
            pstmt.setString(3, User_fname);
            pstmt.setString(4, User_lname);
            pstmt.setString(5, User_gender);
            pstmt.setString(6, User_address);
            pstmt.setString(7, User_type);
            pstmt.setString(8, User_id);
            // updateUser 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void insertUser(String User_id, String User_tel, String User_password, String User_fname, String User_lname, String User_gender, String User_address, String User_type) {
        String sql = "INSERT INTO User(User_id, User_password, User_firstname, User_lastname, User_gender, User_address, User_type, User_tel) VALUES(?,?,?,?,?,?,?,?)";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, User_id);
            pstmt.setString(2, User_password);
            pstmt.setString(3, User_fname);
            pstmt.setString(4, User_lname);
            pstmt.setString(5, User_gender);
            pstmt.setString(6, User_address);
            pstmt.setString(7, User_type);
            pstmt.setString(8, User_tel);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void deleteUser(String User_id) {
        String sql = "DELETE FROM User WHERE User_id = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, User_id);
            // execute the deleteUser statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

}
