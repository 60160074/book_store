/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jainch
 */
public class CustomerDao {

    String[] coln = {"ID","Tel", "Firstname", "Lastname", "Gender", "Type"};
    DefaultTableModel model = new DefaultTableModel(coln, 0) {
    };

    Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:D:/book_store/db/book_store.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return conn;
    }

    public boolean checkAddCustomer(String Customer) {
        String sql = "SELECT Customer_id FROM Customer";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                String id = rs.getString("Customer_id");
                return Customer.equals(id);
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public Customer selectForEdit(String customer) {
        String sql = "SELECT Customer_id, Customer_tel, Customer_firstname, Customer_lastname, Customer_gender, Customer_type FROM Customer";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Customer_id");
                String Tel = rs.getString("Customer_tel");
                String Firstname = rs.getString("Customer_firstname");
                String Lastname = rs.getString("Customer_lastname");
                String Gender = rs.getString("Customer_gender");
                String Type = rs.getString("Customer_type");
                if (customer.equals(ID)) {
                    Customer arr = new Customer(ID, Tel, Firstname, Lastname, Gender, Type);
                    return arr;
                }

            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public DefaultTableModel resetModel(){
        model.setRowCount(0);
        return model;
    }
    
    public DefaultTableModel searchByFname(String fname) {
        String sql = "SELECT Customer_id, Customer_tel, Customer_firstname, Customer_lastname, Customer_gender, Customer_type FROM Customer WHERE Customer_firstname = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, fname);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Customer_id");
                String Tel = rs.getString("Customer_tel");
                String Firstname = rs.getString("Customer_firstname");
                String Lastname = rs.getString("Customer_lastname");
                String Gender = rs.getString("Customer_gender");
                String Type = rs.getString("Customer_type");
                model.addRow(new Object[]{ID, Tel, Firstname, Lastname, Gender, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public DefaultTableModel searchByTel(String tel) {
        String sql = "SELECT Customer_id, Customer_tel, Customer_firstname, Customer_lastname, Customer_gender, Customer_type FROM Customer WHERE Customer_tel = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, tel);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Customer_id");
                String Tel = rs.getString("Customer_tel");
                String Firstname = rs.getString("Customer_firstname");
                String Lastname = rs.getString("Customer_lastname");
                String Gender = rs.getString("Customer_gender");
                String Type = rs.getString("Customer_type");
                model.addRow(new Object[]{ID, Tel, Firstname, Lastname, Gender, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public DefaultTableModel searchByType(String type) {
        String sql = "SELECT Customer_id, Customer_tel, Customer_firstname, Customer_lastname, Customer_gender, Customer_type FROM Customer WHERE Customer_type = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, type);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Customer_id");
                String Tel = rs.getString("Customer_tel");
                String Firstname = rs.getString("Customer_firstname");
                String Lastname = rs.getString("Customer_lastname");
                String Gender = rs.getString("Customer_gender");
                String Type = rs.getString("Customer_type");
                model.addRow(new Object[]{ID, Tel, Firstname, Lastname, Gender, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }
    
    public DefaultTableModel selectForShowTable() {
        String sql = "SELECT Customer_id, Customer_tel, Customer_firstname, Customer_lastname, Customer_gender, Customer_type FROM Customer";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Customer_id");
                String Tel = rs.getString("Customer_tel");
                String Firstname = rs.getString("Customer_firstname");
                String Lastname = rs.getString("Customer_lastname");
                String Gender = rs.getString("Customer_gender");
                String Type = rs.getString("Customer_type");
                model.addRow(new Object[]{ID, Tel, Firstname, Lastname, Gender, Type});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public void updateUser(String Customer_id, String Customer_tel, String Customer_fname, String Customer_lname, String Customer_gender, String Customer_type) {
        String sql = "UPDATE User SET Customer__tel = ? , " + "Customer_firstname = ? ,"
                + "Customer_lastname = ? ," + "Customer_gender = ? ," + "Customer_type = ?"
                + "WHERE Customer_id = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, Customer_tel);
            pstmt.setString(2, Customer_fname);
            pstmt.setString(3, Customer_lname);
            pstmt.setString(4, Customer_gender);
            pstmt.setString(5, Customer_type);
            pstmt.setString(6, Customer_id);
            // updateUser 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void insertCustomer(String Customer_id, String Customer_tel, String Customer_fname, String Customer_lname, String Customer_gender, String Customer_type) {
        String sql = "INSERT INTO User(User_id, User_firstname, User_lastname, User_gender, User_type, User_tel) VALUES(?,?,?,?,?,?)";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, Customer_id);
            pstmt.setString(2, Customer_fname);
            pstmt.setString(3, Customer_lname);
            pstmt.setString(4, Customer_gender);
            pstmt.setString(5, Customer_type);
            pstmt.setString(6, Customer_tel);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void deleteCustomer(String Customer_id) {
        String sql = "DELETE FROM Customer WHERE Customer_id = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, Customer_id);
            // execute the deleteCustomer statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

}
